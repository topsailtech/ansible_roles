# Ansible role installing NGINX config files for a specific rails application

## Important points

- This role creates a `web_root` variable that defaults to `'{{ app_dir }}/current/public'`  This assumes the app deploy will use the ansible deploy_helper.new_release_path etc pattern

- The default `app-nginx.conf.j2` template in this role is probably good for 95+% of apps. If you need to customize the conf template you should create `app-nginx.conf.j2` in the root `templates` folder. If that template file exists, this role will use that template.

- The role template references several default ansible variables. They should be ok and not need overriding unless necessary:
  + web_root: '{{ app_dir }}/current/public'
