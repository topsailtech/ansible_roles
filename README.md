# Topsail ansible_roles

## Using shared roles

1. Copy [setup_ansible_roles.yml](setup_ansible_roles.yml) into your project's `ansible` folder

2. run `ansible-playbook setup_ansible_roles.yml`.
   This will clone all shared roles into your project's `tmp/ansible_roles` folder

3. in your ansible playbooks you can now utilize such shared role via

```yml
         roles:
            - role: ../tmp/ansible_roles/the_shared_role_name
```

4. To always use the latest roles, add this to the top of your `provision.yml` file above the hosts line:
```
- hosts: localhost
  connection: local
  roles:
    - ../tmp/ansible_roles/ensure_latest_ansible_roles
```


## Required group_vars
A good starting point is using [/group_vars/all.yml](group_vars_all.yml).

You need to provide variables in the `Required` section:
* app_name

You should review (and possibly overwrite) variables int the `Shared Defaults` section of that group_vars/all.yml file, especially:
* app_user - defaults to {{ app_name }}
* app_dir  - defaults to /opt/{{app_name}}

Individual role default variables should be overwritten in the `Explicit Overrides` section

## Inventory specific variables

The rails environment must be defined specific to the inventory file:

* rails_env  - Set this to production, staging, etc

In addition, you may wish to override the default name used for the SSL certificate files and the host name in the nginx, postfix, and ssl_server_certificate roles. The default value is the inventory_hostname. In some cases, such as with Duke's VPN accessible servers, the inventory_hostname we have to use is not a fully qualified domain name, but instead something like `vlp-coach.dhemgt`. The `server_dns_name` variable lets you define it to what it should be for nginx such as `coach.duhs.duke.edu`

This variable should be defined in the appropriate inventory file. For example, in COACH production.yml it is:
```
server_dns_name: 'coach.duhs.duke.edu'`
```

Refer to the SSL Certificates and keys section below for details on storing certificates and keys.

The inventory .yml is also where you can tweak passenger config settings, such as:
```
passenger_min_instances: 10
passenger_max_pool_size: 60
```

## Using the Encrypted Vault

See [README_vaults.md](./README_vaults.md)


## SSL Certificates and keys

Our convention is to store SSL certs in clear text in the `ansible/files` directory. Furthermore, you should add any intermediate certificates to the file and give it a `.pem` file extension. It is helpful to add a comment line (beginning with #) to the .pem file above the `-----BEGIN CERTIFICATE-----` line to visually identify the certificate name, e.g. `# casee.dev certificate` or `# USERTrust RSA Certification Authority.cer`

The `ssl_server_certificate` role fetches the certificate from `'files/{{ ssl_certificate_file }}'` and the key file from the vault using `'{{ vault.ssl_keys[ssl_certificate_key_file] }}'` As explained above in `Inventory specific variables` you may need to override the names in the vars `ssl_certificate_file` and `ssl_certificate_key_file`

The generated SSL key file should be securely stored in the Ansible vault. The convention used to store the key is as follows:
```
vault:
  ssl_keys:
    "casee.dev.key":      "-----BEGIN RSA PRIVATE KEY-----\nMIIExx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx=\n-----END RSA PRIVATE KEY-----\n"
    "casee.software.key": "-----BEGIN RSA PRIVATE KEY-----\nMIIExx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx\nxx=\n-----END RSA PRIVATE KEY-----\n"
```

Notice that we join the multiple lines of the key file content into one string with \n newline characters.


## How Tos

### Initialize a Digital Ocean Droplet after creation

With a brand spanking new Digital Ocean droplet, the first thing to do is initialize topsail users and sshd security.
See: https://www.digitalocean.com/community/tutorials/initial-server-setup-with-centos-7

The new droplet will have Richard's ssh key in place for initial root access. Run the `init_sudoer_users` playbook to create topsail user accounts and set sshd security. If anyone other than Richard or Jan are to run this, we must add their ssh key in DigitalOcean before creating the droplet.

```
ansible-playbook -i staging -u root ../tmp/ansible_roles/init_sudoer_users.yml
```

If you try to run this again it should fail because root login is now disabled. So finish it with topsail provision.

### Provision a server
```
ansible-playbook -i staging -u whatever_app_user provision.yml
```


### Ansible Cheat Sheet

- Note the -i (inventory) option specifies which server/hosts inventory to configure.
- The trailing initialize.yml or provision.yml file is the playbook to execute.
  There will likely be a deploy.yml playbook soon.
