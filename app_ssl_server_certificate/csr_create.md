# Generating a CSR

## Create a config file: *my_csr.cnf*
```
[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn

[dn]
C = US
ST = NC
L = Durham
O = The Law Offices of James Scott Farrin
CN = casee.dev

[v3_req]
subjectAltName = @alt_names

[alt_names]
DNS.1 = casee.dev
DNS.2 = www.casee.dev
```

## Create CSR
```
openssl req -new -newkey rsa:2048 -config my_csr.cnf -nodes -keyout casee.dev.key -out casee.dev.csr
```


# Viewing CSRs and Certificates

## View CSR
```
openssl req -noout -text -in casee.dev.csr
```

## View Certificate
```
openssl x509 -noout -text -in casee.dev.pem
```

## Validate that a key matches the certificate
```
# Print the md5 hash of the SSL Certificate modulus
openssl x509 -noout -modulus -in domain.name.crt | openssl md5

# Print the md5 hash of the CSR modulus:
openssl req -noout -modulus -in domain.name.csr | openssl md5

# Print the md5 hash of the Private Key modulus:
openssl rsa -noout -modulus -in domain.name.key | openssl md5
```

If the md5 hashes are the same, then the files (SSL Certificate, Private Key and CSR) are compatible.

One liner:
```
export CERT_FILE=something.com.crt
export KEY_FILE=something.com.key
diff <(openssl x509 -noout -modulus -in $CERT_FILE | openssl md5) <(openssl rsa -noout -modulus -in $KEY_FILE | openssl md5)
```
