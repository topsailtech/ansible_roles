This role installs server SSL certificates.

If no "real" SSL certificate is found, it generates a self signed certificate and installs it.

If you have a real SSL certificate,
- add the pem file to your 
  `{{ playbook_dir }}/files/{{ ssl_certificate_file }}`
- add the private key to vault:
  - the vault must be in file `group_vars/vault.yml`
  - the key must reside in `vault.ssl_keys[ssl_certificate_key_file]` and be a quoted one-liner with "\n" instead of LFs
  
  ```
    vault:
      ssl_keys:
        "my_app_host_name.com.key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhk....\n-----END PRIVATE KEY-----\n"
  ````
