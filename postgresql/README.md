# postgresql ansible role

## Configure

The `defaults/main.yml` file in this role rely on the following default values:

```
postgresql_major_version: 12
postgis_major_version: 3
```

You can override these versions by placing these variables in the Explicit Overrides section of `group_vars/all.yml`

Notes:
- This role has been tested on Centos 7 with Postgres 11 and 12
- Postgres 12 will install on Centos 6 but the Postgis extension will not work
- Use `postgis_major_version` of 3 for Postgres 12, and use 2 for Postgres <= 11
