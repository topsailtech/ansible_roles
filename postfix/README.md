# Ansible Role: Postfix

Installs postfix on RedHat/CentOS or Debian/Ubuntu.


## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    postfix_config_file: /etc/postfix/main.cf

The path to the Postfix `main.cf` configuration file.

    postfix_service_state: started
    postfix_service_enabled: true

The state in which the Postfix service should be after this role runs, and whether to enable the service on startup.

    postfix_soft_bounce: "no"
    postfix_mydestination: ""
    postfix_mynetworks_style: "host"
    postfix_inet_interfaces: "loopback-only"
    postfix_inet_protocols: "all"

Note: these defaults are designed to lock down Postfix for outgoing mail only from the localhost.

Options for these values are described in the `main.cf` file.

## Using a relay mail server

If the `postfix_relayhost` variable is defined, the postfix `relayhost` settting will be configured in the main.cf template to forward mail to this relay host instead of delivering directly.

```
relayhost = relay.some_school.edu
```

## Example Playbook

    - hosts: all
      roles:
        - postfix

