# Ansible app passenger_nginx role

This role must come after the generic passenger_nginx role which installs the passenger version of nginx. This role configures the app specific app-nginx.conf and ssl parts

## Important points

- This role creates a `web_root` variable that defaults to `'{{ app_dir }}/current/public'`  This assumes the app deploy will use the ansible deploy_helper.new_release_path etc pattern

- The default `app-nginx.conf.j2` template in this role is probably good for 95+% of apps. If you need to customize the conf template you should create `app-nginx.conf.j2` in the root `templates` folder. If that template file exists, this role will use that template.

- The role template references several default ansible variables. They should be ok and not need overriding unless necessary:
  + web_root: '{{ app_dir }}/current/public'
  + passenger_app_ruby: '/usr/local/rbenv/versions/{{ ruby_version }}/bin/ruby'

## Using Passenger Enterprise (or not)

The default app-nginx.conf.j2 template in this role creates a location section to configure action cable requests. This config uses a Passenger directive `passenger_max_request_time 0;` that is only available in Passenger Enterprise. If you do not plan to use Enterprise, then you should define your own overriding `app-nginx.conf.j2` in your Ansible templates folder, per the above instructions.
