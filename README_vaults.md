# Using Ansible Vault to Encrypt Secrets

## Get the shared ansible vault password that we use and save it locally

The first thing you need is to get the shared password that we use to encrypt and decrypt vaults. We are using rails' `config/master.key` for this.

You should configure your shell to use that key unless you want to re-enter the file path over and over again
```
export ANSIBLE_VAULT_PASSWORD_FILE=../config/master.key
```

## Create a totally encrypted vault file containing vars

```
ansible-vault create group_vars/vault.yml
```

View or edit the encrypted contents of an ansible vault file
```
ansible-vault view group_vars/vault.yml
ansible-vault edit group_vars/vault.yml
```

To change the vault password:
```
ansible-vault decrypt group_vars/vault.yml --ask-vault-pass

ansible-vault encrypt group_vars/vault.yml
```

or (if you want to specify a password file):
```
ansible-vault encrypt group_vars/vault.yml --vault-password-file=new_ansible_vault_pass.txt
```

### Vault file pros and cons

Pros:
 * The vault view or edit command shows all secrets in one place (similar to Rails Credentials)

Cons:
 * you can not even see the keys unless you decrypt the vault
 * The encrypted vault file has to be explicitly enabled in the tasks file

```
- name: Include encrypted vault variables
  include_vars:
    file: group_vars/vault.yml
```

## Encrypt strings to be included in unencrypted vars files

Encrypt a single string as a variable to be added to the all.yml group vars file
```
ansible-vault encrypt_string 'sample_encrypted_secret' -n sample_secret_key --vault-password-file=~/.ansible_vault_pass.txt >> group_vars/all.yml
```

To view an encrypted string in a yml file:
```
ansible localhost -m debug -a var='sample_secret_key' -e "@group_vars/all.yml" --vault-password-file=~/.ansible_vault_pass.txt
```

### Encrypted strings pros and cons

Pros:
 * All vars are directly in the group_vars file - no need to include a vault.yml
 * all keys for encrypted vars are discoverable, without having to decrypt anything

Cons:
 * Must run the playbook in debug mode (see above) to view decrypted strings
 * awkward way to add vars

## Additional info

To do more such as decrypt, re-encrypt and all that see https://docs.ansible.com/ansible/latest/cli/ansible-vault.html#view
