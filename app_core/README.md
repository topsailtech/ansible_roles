# init_service_account

This role creates an app_user service account, mainly to be used to run the application.

This account will NOT have sudo access. Individuals must login using their own account to gain elevated access.
