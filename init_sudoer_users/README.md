# init_sudoer_users

This role is primarily used for new Digital Ocean droplets that are intiialized with a root account.

So the first task is to create our sudoer user accounts and then disable further root access.

## Vars Configuration

See [defaults/main.yml](defaults/main.yml) for default users to be configured.

You can overwrite this in your group_vars/all.
