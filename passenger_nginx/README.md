# Ansible generic passenger_nginx role

This role must come before the app_generic _passenger_nginx role.
This installs the passenger version of nginx.
The app_passenger role configures the app specific app-nginx.conf and ssl parts
